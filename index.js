const express = require('express')
const hbs =  require('express-handlebars');
const bcrypt = require("bcrypt");
const session = require('express-session')
const mysql = require('mysql');
flash = require('express-flash');
let name_temp = 0;
let user_exist = 1;

const conn = mysql.createConnection({
        host: "localhost",
        user: "root",
        database: "node_js",
        password: ""
});

conn.connect (err => {
    if (err) {
        console.log(err);
        return err;
    }
    else {
        console.log('Database OK ');
    }
});

app = express();

app.use(
    session({
        resave: true,
        saveUninitialized: true,
        secret:"superstar",
        cookie: { secure: false},
    })
);
app.use(flash());
app.use(express.urlencoded ({ extended: false }))
app.use(express.static('public'))
app.use('/css', express.static(__dirname + 'public/css'))

app.set('views', './views');
app.set('view engine', 'hbs');
app.engine('hbs', hbs.engine({
    extname: 'hbs',
    defaultLayout: 'layout',
    layoutsDir: __dirname + '/views/layout/',
    partialsDir: __dirname + '/views/partials'
}));

function getUsers(name){
    if (isNaN(name)){
        conn.query("SELECT * FROM users WHERE name = "+"'"+name+"'", function( err , res ){
            if (res[0]){
                if (res[0]['name']){
                    user_exist = 1;
                }
            }
        });
    }
    else {
        conn.query("SELECT * FROM users WHERE id = "+"'"+name+"'", function( err , res ){
            if (res[0]){
                if (res[0]['name']){
                    user_exist = 1;
                }
            }
        });
    }
}

//Funks
app.post('/register', async (req, res, next) =>{
    getUsers(req.body.name);
    if (user_exist <= 0 ){
        const crypt_pass = await bcrypt.hash(req.body.password, 10)
        var date = new Date().toISOString().
        replace(/T/, ' ').      // replace T with a space
        replace(/\..+/, '')     // delete the dot and everything after
        let query = "INSERT INTO USERS (name, password, created) VALUES ("+"'"+req.body.name+"','"+crypt_pass+"','"+date+"')";
        conn.query(query);
        req.flash('info', ['Done! Now you can log in']);
        req.flash('type', ['success']);
        res.redirect('/login');
    }
    else {
        req.flash('info', ['User already exists, try to login instead']);
        req.flash('type', ['warning']);
        res.redirect('/register');
    }
});

app.post('/login', async (req_b, res) =>{
    var query = "SELECT * FROM users WHERE name = "+"'"+req_b.body.name+"'";
    conn.query(query, (err, result)=>{
        if (result && result.length !== 0){
        bcrypt.compare(req_b.body.password, result[0]['password'], function (err, res_b) {
            if (!err && res_b===true){
                name_temp = result[0]['id'];
                user_exist = 0;
                req_b.flash('info', ['Successfully logged in']);
                req_b.flash('type', ['success']);
                 res.redirect('/');
            }
            else{
                req_b.flash('info', ['Wrong password']);
                req_b.flash('type', ['danger']);
                  res.redirect('/login')
            }
        });
        }
        else {
            req_b.flash('info', ['User does not exist']);
            req_b.flash('type', ['danger']);
             res.redirect('/login')
        }
    });
});

//Routs
app.get('/', (req, res) => {
    if (!name_temp || name_temp===0){
         res.redirect('/login')
    }
    conn.query("SELECT * FROM users", (err, result)=>{
        if (!err){
            res.render("index", { title: "Home", users: result});
        }
    });

});

app.get('/delete/:id', function(req, res, next) {
    var id= req.params.id;
    var sql = 'DELETE FROM users WHERE id = ?';
    conn.query(sql, [id], function (err, data) {
        if (err) throw err;
        console.log(data.affectedRows + " record(s) updated");
    });
    if (id === name_temp){
        console.log('asd');

        res.redirect('/logout')
    }
    res.redirect('/');

});

app.get('/logout', (req, res) => {
    getUsers(name_temp)
    name_temp = 0;
    req.flash('info', ['Successfully logged out']);
    req.flash('type', ['success']);
     res.redirect('/login');

});

app.get('/register', (req, res) => {
    if (name_temp!==0){
         res.redirect('/')
    }
    res.render("register", { title: "Register" });
});

app.get('/login', (req, res) => {
    if (name_temp!==0){
         res.redirect('/')
    }
    res.render("login", { title: "Login" });
});

app.listen(3000, () => {
    console.log("Listening on port 3000");
});